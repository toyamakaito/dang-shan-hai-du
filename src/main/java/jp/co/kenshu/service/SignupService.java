package jp.co.kenshu.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.co.kenshu.mapper.SignupMapper;

@Service
public class SignupService {

    @Autowired
    private SignupMapper signupMapper;
    @Transactional
    public void getSignup(String name,String account,String password) {
    	signupMapper.getTest(name,account,password);
    }
}