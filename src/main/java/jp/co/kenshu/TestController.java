package jp.co.kenshu;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import jp.co.kenshu.dto.test.TestDto;
import jp.co.kenshu.form.SignupForm;
import jp.co.kenshu.form.TestForm;
import jp.co.kenshu.service.SignupService;
import jp.co.kenshu.service.TestService;

@SessionAttributes("loginUser")
@Controller
public class TestController {

	@Autowired
	private TestService testService;

	@Autowired
	private SignupService signupService;

	@RequestMapping(value = "/show", method = RequestMethod.GET)
	public String showMessage(Model model) {
		TestForm form = new TestForm();
		model.addAttribute("testForm", form);
		return "showMessage";
	}

	@RequestMapping(value = "/show", method = RequestMethod.POST)
	public String getFormInfo(@ModelAttribute TestForm form, Model model) {
		TestDto test = testService.getTest(form.getAccount(), form.getPassword());
		model.addAttribute("loginUser", test);
		return "redirect:top";
	}

	@RequestMapping(value = "/top", method = RequestMethod.GET)
	public String top() {
		return "top";
	}

	@RequestMapping(value = "/signup", method = RequestMethod.GET)
	public String signUp(Model model) {
		SignupForm signup = new SignupForm();
		model.addAttribute("signUp", signup);
		return "signup";
	}

	@RequestMapping(value = "/signup", method = RequestMethod.POST)
	public String signUp2(@ModelAttribute SignupForm signup, Model model) {
		signupService.getSignup(signup.getAccount(), signup.getName(), signup.getPassword());
		return "redirect:top";
	}
}