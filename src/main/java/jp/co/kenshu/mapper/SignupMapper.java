package jp.co.kenshu.mapper;

import jp.co.kenshu.entity.Test;

public interface SignupMapper {
    Test getTest(String name,String account,String password);

}