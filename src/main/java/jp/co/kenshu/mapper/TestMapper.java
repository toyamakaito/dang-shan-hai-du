package jp.co.kenshu.mapper;

import jp.co.kenshu.entity.Test;

public interface TestMapper {
    Test getTest(String account,String password);

}