<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
    <head>
        <meta charset="utf-8">
        <title>Welcome</title>
    </head>
    <body>
        <form:form modelAttribute="signUp">
            アカウント名<form:input path="account" />
            名前<form:input path="name" />
            パスワード<form:input path="password" />
            <input type="submit">
        </form:form>
    </body>
</html>