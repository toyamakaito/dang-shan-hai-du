<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
<meta charset="utf-8">
<title>Welcome</title>
</head>
<body>
	<div class="head">
		<a href="newMessage">新規投稿</a> <a href="signup">ユーザー新規登録画面</a> <a
			href="logout">ログアウト</a> <a href="index.jsp">HOME</a><br />
	</div>
	<h1>${message}</h1>
	<h2>${loginUser.name}</h2>
</body>
</html>